-- 1.
SELECT customerName FROM customers where country = "Philippines";

-- 2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3.
SELECT productName,MSRP FROM products where productName = "The Titanic";

-- 4.
SELECT firstName,lastName from employees where email = "jfirrelli@classicmodelcars.com";

-- 5.
SELECT customerName FROM customers WHERE state IS NULL;

-- 6.
SELECT firstName,lastName, email FROM employees WHERE lastName="Patterson" AND firstName = "Steve";

-- 7.
SELECT customerName,country,creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000; 

-- 8.
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9.
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10.
SELECT DISTINCT country FROM customers;

-- 11.
SELECT DISTINCT status FROM orders;

-- 12.
SELECT customerName,country FROM customers WHERE country ="USA" OR country ="France" OR country ="Canada";

-- 13.
SELECT firstName, lastName, offices.city FROM employees
JOIN offices ON employees.officeCode = offices.officeCode WHERE employees.officeCode =5;

-- 14.
SELECT customerName from customers
join employees on employees.employeeNumber = customers.salesRepEmployeeNumber where employees.firstName = "Leslie" AND employees.lastName = "Thompson" ;

-- 15.
SELECT customers.customerName,productName FROM products
JOIN orderdetails on orderdetails.productCode = products.productCode
JOIN orders on orderdetails.orderNumber = orders.orderNumber
JOIN customers on customers.customerNumber = orders.customerNumber WHERE 
customerName = "Baane Mini Imports";

-- 16.
SELECT employees.firstName,employees.lastName,customers.customerName,offices.country FROM offices
JOIN employees on offices.officeCode = employees.officeCode
JOIN customers on customers.salesRepEmployeeNumber = employees.employeeNumber WHERE customers.country = offices.country;

-- 17.
SELECT products.productName, products.quantityInStock, products.productLine FROM productlines
JOIN products on products.productLine = productlines.productLine WHERE products.quantityInStock < 1000 AND products.productLine = "planes";

-- 18.
SELECT customerName,phone FROM customers WHERE phone LIKE "+81%";